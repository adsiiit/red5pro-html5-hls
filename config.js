var config = {};

//Stream settings
config.streamUrl = "http://35.154.70.148";
config.streamPort = 5080;
config.streamWebSocketPort = 6262;
config.streamContext = "live";


//Port to run the server
config.port = 5050;

module.exports = config;